from django.urls import path
from userhandle.views import RegisterUser, AuthenticationUser
app_name = "User"

urlpatterns = [
    path("register", RegisterUser.as_view(), name = "register_api"),
    path("authenticated", AuthenticationUser.as_view(), name = "authenticate")
]