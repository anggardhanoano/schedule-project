from rest_framework import serializers
from userhandle.models import UserModel

class UserRegisterSerializers(serializers.ModelSerializer):

    class Meta:
        model = UserModel
        fields = "__all__"


    def get_user_data(self, request):
        try:
            data = []
            objek = UserModel.objects.all()
            for i in range(len(objek)):
                data.append({
                    "email" : objek[i].email
                })
            return data
        except:
            return None
