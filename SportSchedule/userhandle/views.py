from django.shortcuts import render
from rest_framework import status
from userhandle.models import UserModel, UserManager
from rest_framework.views import APIView
from rest_framework.response import Response
from userhandle.serializers import UserRegisterSerializers
from rest_framework.permissions import IsAuthenticated  
from rest_framework.renderers import JSONRenderer


class RegisterUser(APIView):

    def post(self, request):
        try :
            email = request.data["email"]
            username = request.data["username"]
            pass1 = request.data["password1"]
            pass2 = request.data["password2"]
            user_picture = request.data["user_picture"]
            if (pass1 == pass2):
                user = UserModel.objects.create(
                    email = email,
                    username = username,
                    user_picture = user_picture
                )
                user.set_password(pass1)
                user.save()
                serializer = UserRegisterSerializers(user)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else :
                raise Exception()
        
        except Exception as e:
            return Response({"message": str(e)}, status.HTTP_400_BAD_REQUEST)

class AuthenticationUser(APIView):

    permission_classes = {IsAuthenticated,}

    def get(self, request):

        data = []
        user = UserModel.objects.all()
        for i in user:
            data.append(UserRegisterSerializers(i).data)
        return Response(data)

